NETWORK_NAME = taskify-network
TEST_IMAGE_NAME = taskify_backend_test

build: start-db start-backend

test: start-db run-tests

start-db:
	docker-compose up -d postgres

start-backend:
	docker-compose up -d --build web

run-tests: build-test-image create-network
	docker run --env-file ./.docker.env --link taskify-db --net $(NETWORK_NAME) -i $(TEST_IMAGE_NAME)

build-test-image:
	docker build -f Dockerfile.test -t $(TEST_IMAGE_NAME) .

create-network:
	docker network create $(NETWORK_NAME) || true

clean:
	docker-compose down
