# Small Django app for task management (backend only)
###  Prerequisites to run app:
- docker
- docker-compose
- GNU Make [optional]

### For running app:
	docker-compose up -d --build

Alternatively, with make:

	make

### For running tests:
	docker-compose up -d postgres
	docker network create taskify-network || true
	docker build -f Dockerfile.test -t taskify_backend_test .
	docker run --env-file ./.docker.env --link taskify-db --net taskify-network -i taskify_backend_test

Alternatively, with make:

	make test

*Note:* You can find postman from [here.](https://www.getpostman.com/collections/290cf70bfcebe1b60145)
