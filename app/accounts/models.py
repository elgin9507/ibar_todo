from django.contrib.auth.models import AbstractUser
from django.db import models


class TaskifyUser(AbstractUser):
    USERNAME_FIELD = "username"
    EMAIL_FIELD = "email"
    DEFAULT_PASSWORD = "rg5t7k"

    username = models.CharField(max_length=50, unique=True)
    email = models.EmailField(unique=True)
    first_name = models.CharField(max_length=50, null=True)
    last_name = models.CharField(max_length=50, null=True)

    @property
    def full_name(self):
        if self.first_name and self.last_name:
            return self.get_full_name()

        return ""
