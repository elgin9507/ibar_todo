from django.contrib.auth import authenticate, get_user_model
from django.contrib.auth.password_validation import validate_password
from django.db import transaction
from rest_framework import serializers

from core import serializers as core_serializers


User = get_user_model()


class SignUpSerializer(serializers.ModelSerializer):
    organization = core_serializers.OrganizationSerializer(
        many=False, required=True, write_only=True
    )

    class Meta:
        model = User
        fields = ("username", "email", "password", "organization")
        extra_kwargs = {"password": {"write_only": True}}

    def validate_password(self, password):
        validate_password(password)


class UserReadSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("id", "username", "email", "full_name")


class LoginSerializer(serializers.Serializer):
    username = serializers.CharField(required=True)
    password = serializers.CharField(required=True)

    def validate_password(self, password):
        validate_password(password)

        return password

    def validate(self, data):
        user = authenticate(**data)
        if user is None:
            raise serializers.ValidationError("Wrong credentials")

        return user
