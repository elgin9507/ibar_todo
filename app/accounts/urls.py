from django.urls import path

from accounts import views

app_name = "accounts"

urlpatterns = [
    path("signup/", views.SignUpView.as_view(), name="signup"),
    path("login/", views.LoginView.as_view(), name="login"),
    path("profile/", views.UserProfileView.as_view(), name="profile"),
]
