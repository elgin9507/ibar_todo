from django.core.validators import ValidationError
from django.utils.translation import gettext as _


class AlphaNumericPasswordValidator:
    """
    Validate whether the password is alphanumeric.
    """

    def validate(self, password, user=None):
        if password.isdigit() or password.isalpha():
            raise ValidationError(
                _("This password is not alphanumeric"), code="password_not_alphanumeric"
            )

    def get_help_text(self):
        return _("Your password should contain both letters and numbers")
