# Generated by Django 3.1.7 on 2021-03-10 20:53

from django.db import migrations, models
import django.db.models.deletion


class Migration(migrations.Migration):

    dependencies = [
        ('core', '0002_auto_20210310_1746'),
    ]

    operations = [
        migrations.CreateModel(
            name='Task',
            fields=[
                ('id', models.AutoField(auto_created=True, primary_key=True, serialize=False, verbose_name='ID')),
                ('title', models.CharField(max_length=200)),
                ('description', models.TextField(max_length=20000)),
                ('deadline', models.DateField()),
                ('status', models.CharField(choices=[('s1', 'to do'), ('s2', 'in progress'), ('s3', 'done')], max_length=2)),
                ('organization', models.ForeignKey(on_delete=django.db.models.deletion.CASCADE, related_name='tasks', to='core.organization')),
            ],
        ),
    ]
