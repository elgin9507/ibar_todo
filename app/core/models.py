from django.conf import settings
from django.db import models
from django.utils.translation import gettext as _

from core.validators import phone_regex


class Organization(models.Model):
    name = models.CharField(max_length=100)
    address = models.CharField(max_length=150)
    phone_number = models.CharField(max_length=15, validators=[phone_regex], null=True)
    admins = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="managed_organizations"
    )
    members = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="organizations"
    )

    def __str__(self):
        return self.name

    def user_is_admin(self, user):
        is_admin = self.admins.filter(pk=user.pk).exists()

        return is_admin

    def user_is_member(self, user):
        is_member = self.members.filter(pk=user.pk).exists()

        return is_member


class Task(models.Model):
    TASK_STATUS_TODO = "s1"
    TASK_STATUS_IN_PROGRESS = "s2"
    TASK_STATUS_DONE = "s3"

    TASK_STATUS_CHOICES = (
        (TASK_STATUS_TODO, _("to do")),
        (TASK_STATUS_IN_PROGRESS, _("in progress")),
        (TASK_STATUS_DONE, _("done")),
    )

    organization = models.ForeignKey(
        "core.Organization", on_delete=models.CASCADE, related_name="tasks"
    )
    title = models.CharField(max_length=200)
    description = models.TextField(max_length=20000)
    deadline = models.DateField()
    status = models.CharField(max_length=2, choices=TASK_STATUS_CHOICES)
    assignees = models.ManyToManyField(
        settings.AUTH_USER_MODEL, related_name="assigned_tasks"
    )

    def __str__(self):
        return self.title

    def save(self, *args, **kwargs):
        if not self.status:
            self.status = self.TASK_STATUS_TODO

        return super().save(*args, **kwargs)
