from django.contrib.auth import get_user_model
from django.core import mail
from rest_framework import serializers

from core.models import Organization, Task

User = get_user_model()


class OrganizationSerializer(serializers.ModelSerializer):
    total_members = serializers.SerializerMethodField()
    members = serializers.SerializerMethodField()

    class Meta:
        model = Organization
        fields = ("id", "name", "address", "phone_number", "total_members", "members")

    def get_total_members(self, organization):
        return organization.members.count()

    def get_members(self, organization):
        from accounts.serializers import UserReadSerializer

        members_data = UserReadSerializer(
            instance=organization.members.all(), many=True
        ).data

        return members_data

    def get_fields(self):
        fields = super().get_fields()
        include_member_details = self.context.get("include_member_details", False)

        if not include_member_details:
            if "total_members" in fields:
                del fields["total_members"]
            if "members" in fields:
                del fields["members"]

        return fields


class OrganizationNewUserSerializer(serializers.ModelSerializer):
    class Meta:
        model = User
        fields = ("username", "first_name", "last_name", "email")
        extra_kwargs = {
            "first_name": {"allow_null": False, "required": True},
            "last_name": {"allow_null": False, "required": True},
        }


class TaskReadSerializer(serializers.ModelSerializer):
    status = serializers.SerializerMethodField()
    assignees = serializers.SerializerMethodField()

    class Meta:
        model = Task
        fields = ("id", "title", "description", "deadline", "status", "assignees")

    def get_status(self, task):
        return task.get_status_display()

    def get_assignees(self, task):
        from accounts.serializers import UserReadSerializer

        assignees_data = UserReadSerializer(
            instance=task.assignees.all(), many=True
        ).data

        return assignees_data


class TaskCreateSerializer(serializers.ModelSerializer):
    class Meta:
        model = Task
        fields = ("title", "description", "deadline", "assignees")
        extra_kwargs = {"assignees": {"allow_empty": True}}

    def create(self, validated_data):
        assigned_users = validated_data.get("assignees", [])
        instance = super().create(validated_data)

        return instance

    def send_assignment_mails(self, users):
        with mail.get_connection() as connection:
            for user in users:
                mail_msg = mail.EmailMessage(
                    subject="New task!",
                    body=f"Dear {user.full_name}, you are assigned to new task",
                    to=[user.email],
                    connection=connection,
                )
                mail_msg.send()

    def to_representation(self, task):
        return TaskReadSerializer(instance=task).data
