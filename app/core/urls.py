from django.urls import path

from core import views

urlpatterns = [
    path(
        "organization/<int:id>/new-user/",
        views.OrganizationAddUserView.as_view(),
        name="organization-new-user",
    ),
    path(
        "organization/<int:id>/add-task/",
        views.TaskCreateView.as_view(),
        name="organization-add-task",
    ),
    path(
        "organization/<int:id>/tasks/",
        views.OrganizationTaskListView.as_view(),
        name="organization-task-list",
    ),
    path(
        "managed-organizations/",
        views.ManagedOrganizationsListView.as_view(),
        name="managed-organizations",
    ),
    path(
        "joined-organizations/",
        views.JoinedOrganizationsListView.as_view(),
        name="joined-organizations",
    ),
]
