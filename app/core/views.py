from django.contrib.auth import get_user_model
from rest_framework import generics, status, views
from rest_framework.exceptions import PermissionDenied
from rest_framework.response import Response
from rest_framework.generics import get_object_or_404

from core.models import Organization
from core.serializers import (
    OrganizationNewUserSerializer,
    OrganizationSerializer,
    TaskCreateSerializer,
    TaskReadSerializer,
)

User = get_user_model()


class OrganizationAddUserView(views.APIView):
    def post(self, request, *args, **kwargs):
        organization = get_object_or_404(Organization, pk=kwargs["id"])
        if not organization.user_is_admin(request.user):
            raise PermissionDenied("You are not admin of this organization")

        serializer = OrganizationNewUserSerializer(data=request.data)
        serializer.is_valid(raise_exception=True)
        serializer.validated_data.update(password=User.DEFAULT_PASSWORD)

        user = User.objects.create_user(**serializer.validated_data)
        organization.members.add(user)

        resp_data = OrganizationSerializer(
            instance=organization, context={"include_member_details": True}
        ).data

        return Response(resp_data, status=status.HTTP_200_OK)


class TaskCreateView(generics.CreateAPIView):
    serializer_class = TaskCreateSerializer

    def post(self, request, *args, **kwargs):
        self.organization = get_object_or_404(Organization, pk=kwargs["id"])
        if not self.organization.user_is_admin(request.user):
            raise PermissionDenied("You are not admin of this organization")

        return super().post(request, *args, **kwargs)

    def perform_create(self, serializer):
        serializer.validated_data.update(organization=self.organization)

        super().perform_create(serializer)


class OrganizationTaskListView(generics.ListAPIView):
    serializer_class = TaskReadSerializer

    def get_queryset(self):
        organization = get_object_or_404(Organization, pk=self.kwargs["id"])
        if organization.user_is_admin(self.request.user) or organization.user_is_member(
            self.request.user
        ):
            return organization.tasks.all()

        raise PermissionDenied("Insufficient permission")


class ManagedOrganizationsListView(generics.ListAPIView):
    serializer_class = OrganizationSerializer

    def get_queryset(self):
        return self.request.user.managed_organizations.all()


class JoinedOrganizationsListView(generics.ListAPIView):
    serializer_class = OrganizationSerializer

    def get_queryset(self):
        return self.request.user.organizations.all()
