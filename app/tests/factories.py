import string

import factory
from factory.django import DjangoModelFactory
from rest_framework.authtoken.models import Token

from accounts import models as am
from core import models as cm


class TaskifyUserFactory(DjangoModelFactory):
    class Meta:
        model = am.TaskifyUser

    username = factory.Faker("first_name")
    email = factory.Faker("ascii_email")
    first_name = factory.Faker("first_name")
    last_name = factory.Faker("last_name")


class TokenFactory(DjangoModelFactory):
    class Meta:
        model = Token

    user = factory.SubFactory("tests.factories.TaskifyUserFactory")


class OrganizationFactory(DjangoModelFactory):
    class Meta:
        model = cm.Organization

    name = factory.Faker("pystr", max_chars=100)
    address = factory.Faker("address")
    phone_number = factory.Faker(
        "pystr_format", string_format="+############", letters=string.digits
    )


class TaskFactory(DjangoModelFactory):
    class Meta:
        model = cm.Task

    organization = factory.SubFactory("tests.factories.OrganizationFactory")
    title = "task title"
    description = "task description"
    deadline = factory.Faker("date")
