from django.test import TestCase

from tests.factories import OrganizationFactory, TaskifyUserFactory


class OrganizationModelTestCase(TestCase):
    def setUp(self):
        self.organization = OrganizationFactory()
        self.user = TaskifyUserFactory()

    def test_user_is_admin(self):
        self.assertFalse(self.organization.user_is_admin(self.user))

        self.organization.admins.add(self.user)

        self.assertTrue(self.organization.user_is_admin(self.user))

    def test_user_is_member(self):
        self.assertFalse(self.organization.user_is_member(self.user))

        self.organization.members.add(self.user)

        self.assertTrue(self.organization.user_is_member(self.user))
