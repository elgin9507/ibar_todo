from django.test import TestCase

from core.models import Task
from tests.factories import TaskFactory


class TaskModelTestCase(TestCase):
    def test_default_status_is_set_on_save(self):
        task = TaskFactory(status=None)

        self.assertIsNotNone(task.status)

    def test_status_not_overridded_by_save_method(self):
        task = TaskFactory(status=Task.TASK_STATUS_IN_PROGRESS)

        self.assertEqual(task.status, Task.TASK_STATUS_IN_PROGRESS)
