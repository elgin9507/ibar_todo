from django.test import TestCase

from tests.factories import TaskifyUserFactory


class UserModelTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user1 = TaskifyUserFactory(first_name="tom", last_name="sawyer")
        cls.user2 = TaskifyUserFactory(first_name=None, last_name="sawyer")
        cls.user3 = TaskifyUserFactory(first_name="tom", last_name=None)

    def test_full_name(self):
        self.assertEqual(self.user1.full_name, "tom sawyer")
        self.assertEqual(self.user2.full_name, "")
        self.assertEqual(self.user3.full_name, "")
