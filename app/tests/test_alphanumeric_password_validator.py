from django.core.validators import ValidationError
from django.test import TestCase

from accounts.validators import AlphaNumericPasswordValidator


class AlphaNumericPasswordValidatorTestCase(TestCase):
    def setUp(self):
        self.alphanumeric_passwd = "325ve5"
        self.alpha_passwd = "eifjm"
        self.numeric_passwd = "43534"

        self.expected_exc_message = "This password is not alphanumeric"

    def test_validation(self):
        try:
            AlphaNumericPasswordValidator().validate(self.alphanumeric_passwd)
        except ValidationError:
            self.fail("validation error raised for valid password")

        with self.assertRaisesMessage(ValidationError, self.expected_exc_message):
            AlphaNumericPasswordValidator().validate(self.alpha_passwd)

        with self.assertRaisesMessage(ValidationError, self.expected_exc_message):
            AlphaNumericPasswordValidator().validate(self.numeric_passwd)
