from django.urls import reverse
from django.test import Client, TestCase
from rest_framework.authtoken.models import Token

from tests.factories import TaskifyUserFactory


class LoginViewTestCase(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = TaskifyUserFactory(username="username")
        cls.user.set_password("pass12")
        cls.user.save()

    def setUp(self):
        client = Client()
        login_url = reverse("accounts:login")
        payload = {"username": "username", "password": "pass12"}
        self.resp = client.post(login_url, payload, content_type="application/json")

    def test_response_is_ok(self):
        self.assertEqual(self.resp.status_code, 200)

    def test_token_created(self):
        self.assertTrue(Token.objects.filter(user=self.user).exists())

    def test_expected_response(self):
        expected_response = {
            "id": self.user.pk,
            "username": "username",
            "email": self.user.email,
            "full_name": self.user.full_name,
            "token": Token.objects.get(user=self.user).key,
        }
        actual_response = self.resp.data

        self.assertDictEqual(expected_response, actual_response)
