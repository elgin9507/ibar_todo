from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from core.models import Task
from tests.factories import OrganizationFactory, TaskifyUserFactory, TokenFactory

User = get_user_model()


class TaskCreateViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.org = OrganizationFactory()
        cls.org_admin = TaskifyUserFactory()
        cls.org.admins.add(cls.org_admin)
        cls.user = TaskifyUserFactory()
        cls.endpoint = reverse("organization-add-task", args=(cls.org.pk,))
        cls.task_data = {
            "title": "task title",
            "description": "task description",
            "deadline": "2021-05-09",
            "assignees": [cls.user.pk],
        }

    def test_non_admin_user(self):
        client = Client()
        token = TokenFactory(user=self.user)
        auth_headers = {"HTTP_AUTHORIZATION": f"Token {token.key}"}
        resp = client.post(self.endpoint, self.task_data, **auth_headers)
        err_message = str(resp.data["detail"])

        self.assertEqual(resp.status_code, 403)
        self.assertEqual(err_message, "You are not admin of this organization")

    def test_admin_user(self):
        client = Client()
        token = TokenFactory(user=self.org_admin)
        auth_headers = {"HTTP_AUTHORIZATION": f"Token {token.key}"}
        resp = client.post(self.endpoint, self.task_data, **auth_headers)

        self.assertEqual(resp.status_code, 201)

        task = Task.objects.filter(title="task title").last()

        self.assertIsNotNone(task)
        self.assertTrue(task.assignees.filter(pk=self.user.pk).exists())
