from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from tests.factories import OrganizationFactory, TaskifyUserFactory, TokenFactory

User = get_user_model()


class OrganizationAddUserViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = TaskifyUserFactory()
        cls.org_admin = TaskifyUserFactory()
        cls.org = OrganizationFactory()
        cls.org.admins.add(cls.org_admin)
        cls.new_user_data = {
            "username": "new_username",
            "email": "new_user@mail.com",
            "first_name": "first name",
            "last_name": "last name",
        }
        cls.endpoint = reverse("organization-new-user", args=(cls.org.pk,))

    def test_non_admin_user_add_user(self):
        client = Client()
        token = TokenFactory(user=self.user)
        auth_header = {"HTTP_AUTHORIZATION": f"Token {token.key}"}
        resp = client.post(self.endpoint, self.new_user_data, **auth_header)
        err_message = str(resp.data["detail"])

        self.assertEqual(resp.status_code, 403)
        self.assertEqual(err_message, "You are not admin of this organization")

    def test_admin_user_add_user(self):
        client = Client()
        token = TokenFactory(user=self.org_admin)
        auth_header = {"HTTP_AUTHORIZATION": f"Token {token.key}"}
        resp = client.post(self.endpoint, self.new_user_data, **auth_header)

        self.assertEqual(resp.status_code, 200)

        new_user = User.objects.filter(username="new_username").last()

        self.assertIsNotNone(new_user)
        self.assertTrue(self.org.members.filter(pk=new_user.pk).exists())
