from django.test import Client, TestCase
from django.urls import reverse

from tests.factories import OrganizationFactory, TaskifyUserFactory, TokenFactory


class OrganizationTaskListViewTest(TestCase):
    @classmethod
    def setUpTestData(cls):
        cls.user = TaskifyUserFactory()
        cls.admin_user = TaskifyUserFactory()
        cls.member_user = TaskifyUserFactory()
        cls.org = OrganizationFactory()

        cls.org.admins.add(cls.admin_user)
        cls.org.members.add(cls.member_user)

        cls.endpoint = reverse("organization-task-list", args=(cls.org.pk,))

    def setUp(self):
        self.client = Client()

    def test_other_user(self):
        auth_header = self._auth_header_for_user(self.user)
        resp = self.client.get(self.endpoint, **auth_header)

        self.assertEqual(resp.status_code, 403)

        err_msg = str(resp.data["detail"])

        self.assertEqual(err_msg, "Insufficient permission")

    def test_member_user(self):
        auth_header = self._auth_header_for_user(self.member_user)
        resp = self.client.get(self.endpoint, **auth_header)

        self.assertEqual(resp.status_code, 200)

    def test_admin_user(self):
        auth_header = self._auth_header_for_user(self.admin_user)
        resp = self.client.get(self.endpoint, **auth_header)

        self.assertEqual(resp.status_code, 200)

    @staticmethod
    def _auth_header_for_user(user):
        token = TokenFactory(user=user)
        auth_header = {"HTTP_AUTHORIZATION": f"Token {token.key}"}

        return auth_header
