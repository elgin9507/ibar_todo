from django.contrib.auth import get_user_model
from django.test import Client, TestCase
from django.urls import reverse

from core.models import Organization

User = get_user_model()


class SignUpViewTestCase(TestCase):
    def setUp(self):
        client = Client()
        endpoint = reverse("accounts:signup")
        payload = {
            "username": "username",
            "email": "user@email.com",
            "password": "12345t",
            "organization": {
                "name": "org llc",
                "address": "org address",
                "phone_number": "+9945724547",
            },
        }
        self.response = client.post(endpoint, payload, content_type="application/json")

    def test_response_is_ok(self):
        self.assertEqual(self.response.status_code, 201)

    def test_user_created(self):
        user = User.objects.filter(username="username").last()

        self.assertIsNotNone(user)

    def test_organization_created(self):
        organization = Organization.objects.filter(
            name="org llc", address="org address"
        ).last()

        self.assertIsNotNone(organization)

    def test_user_is_admin_of_organization(self):
        user = User.objects.get(username="username")
        org = Organization.objects.get(name="org llc")

        self.assertTrue(org.user_is_admin(user))

    def test_expected_response(self):
        expected_resp = {"username": "username", "email": "user@email.com"}
        actual_resp = self.response.data

        self.assertDictEqual(actual_resp, expected_resp)
