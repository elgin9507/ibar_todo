#!/usr/bin/env bash

python app/manage.py makemigrations --merge --noinput
python app/manage.py migrate

exec "$@"