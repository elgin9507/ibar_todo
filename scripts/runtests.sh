#!/usr/bin/env bash

pip install -U pip
pip install -r requirements.txt
coverage run --omit *tests* app/manage.py test -v 2 app/tests
coverage report

exec "$@"